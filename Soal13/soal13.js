import React from 'react';

class Nama extends React.Component {
  render() {
    return <p> <b>  {this.props.nama} </b></p>;
  }
}

class Age extends React.Component {
  render() {
    return <p> {this.props.age} </p>;
  }
}

class Gender extends React.Component {
  render() {
    return <p> {this.props.gender}  </p>;
  }
}

class Profesi extends React.Component {
  render() {
    return <p> {this.props.profession} year old </p>;
  }
}

class Photo extends React.Component {
  render() {
    return <img src={this.props.photo} width="100%"/>;
  }
}

const data = [
 	{name: "John", age: 25, gender: "Male", profession: "Engineer", photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"}, 
 	{name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"}, 
 	{name: "David", age: 30, gender: "Male", profession: "Programmer", photo: "https://media.istockphoto.com/photos/handsome-mexican-hipster-man-sending-email-with-laptop-picture-id1182472756"}, 
 	{name: "Kate", age: 27, gender: "Female", profession: "Model", photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" }
]

class Soal13 extends React.Component {
  render() {
    return (
     <div>
     	<table style={{"text-align":"left", width:"40%", margin:"0 auto 20px", }}>
		<tr>
			<td style={{border: "2px solid", borderRadius: "10px"}}>
				<div> 
					<Photo photo={data[0].photo} />
				</div>
				<div> 
					<Nama nama={data[0].name} />
					<Profesi profession={data[0].profession} />
					<Age age={data[0].age} />
				</div> 
			</td>
			<td style={{border: "2px solid", borderRadius: "10px"}}>
				<div> 
					<Photo photo={data[1].photo} />
				</div>
				<div> 
					<Nama nama={data[1].name} />
					<Profesi profession={data[1].profession} />
					<Age age={data[1].age} />
				</div> 
			</td>
		</tr>
		<tr>
			<td style={{border: "2px solid", borderRadius: "10px"}}>
				<div> 
					<Photo photo={data[2].photo} />
				</div>
				<div> 
					<Nama nama={data[2].name} />
					<Profesi profession={data[2].profession} />
					<Age age={data[2].age} />
				</div> 
			</td>
			<td style={{border: "2px solid", borderRadius: "10px"}}>
				<div> 
					<Photo photo={data[3].photo} />
				</div>
				<div> 
					<Nama nama={data[3].name} />
					<Profesi profession={data[3].profession} />
					<Age age={data[3].age} />
				</div> 
			</td>
		</tr>
        </table>
      </div>
    )
  }
}

export default Soal13