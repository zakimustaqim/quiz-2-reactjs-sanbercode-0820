// SOAL 12
class BangunDatar{
	constructor(nama){
		this._nama = nama;
		this._luas = "";
		this._keliling = "";
	}
	get nama(){
		return this._nama;
	}
	set nama(nama){
		this._nama = nama;
	}
	luas(){
		return this._luas;
	}
	keliling(){
		return this.keliling;
	}
}

class Lingkaran extends BangunDatar{
	constructor(nama){
		super(nama)
	}

	get luas(){
		return this._luas;
	}
	set luas(jari){
		this._luas =  22 / 7 * jari * jari;

	}

	get keliling(){
		return this._keliling;
	}
	set keliling(jari){
		this._keliling =  2* 22 / 7 * jari;

	}

}

class Persegi extends BangunDatar{
	constructor(nama){
		super(nama)
	}
	
	get luas(){
		return this._luas;
	}
	set luas(sisi){
		this._luas = sisi * sisi;
	}

	get keliling(){
		return this._keliling;
	}
	set keliling(sisi){
		this._keliling =  sisi * 4;
	}

}


var bangunDatar = new BangunDatar("Luas Dan Keliling");
console.log(bangunDatar.luas());
console.log(bangunDatar.keliling());

var lingkaran = new Lingkaran("lingkaran", 7);
lingkaran.keliling = 7;
console.log(lingkaran.keliling)
lingkaran.luas = 7;
console.log(lingkaran.luas);

var persegi = new Persegi("persegi");
persegi.keliling = 5;
console.log(persegi.keliling);
persegi.luas = 5;
console.log(persegi.luas);